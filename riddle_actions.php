<?php

require_once 'db.php';

$db = null;

function submit_riddle()
{
    global $db;


    $name = $_POST['name'];
    $description = $_POST['description'];
    $type = $_POST['class'];
    $origin = $_POST['origin'];
    $pcs = $_POST['pcs'];
    $author = $_POST['author'];
    $theme = $_POST['theme'];
    $kind = $_POST['kind'];
    $lang = $_POST['lang'];
    $langdiff = $_POST['langdiff'];
    if (empty($langdiff)) $langdiff = 0;
    $spacediff = $_POST['spacediff'];
    if (empty($spacediff)) $spacediff = 0;
    $timediff = $_POST['timediff'];
    if (empty($timediff)) $timediff = 0;
    $minddiff = $_POST['minddiff'];
    if (empty($minddiff)) $minddiff = 0;

    // load user id
    $query = dotazSQL(' select ID_zberatel from zberatel where email="' . $_COOKIE['user'] . '"', $db);
    if (!$query) {
        echo json_encode(array('status' => 'error', 'sqlerr' => mysql_error($query)));
        die();
    }
    $res = mysql_fetch_array($query);

    if (mysql_num_rows($query) < 1) {
        exit("Uzivatel neznamy");
    }

    $userid = $res[0];

    $id_puzzle = null;
    $id_riddle = null;

    //mysql_begin_transaction($db);

     $query = dotazSQL(' insert into hlavolam (id_vlastnik, nazov, typ, krajina_povodu, obtiaznost_priestorova, obtiaznost_casova, obtiaznost_vedomostna)
                      values ("' . $userid . '","' . $name . '","' . $description . '","' . $origin . '","' . $spacediff . '","' . $timediff . '","' . $minddiff . '")', $db);

    if (!$query) {
       // die("Failed to insert riddle");
    }


    $id = mysql_insert_id();

    if ($type == "puzzle") {
        //load puzzle related values
        //set flag
        $x = dotazSQL(' insert into puzzle ( pocet_dielov, autor, tema)
                 values ( "'. $pcs . '","' . $author . '","' . $theme . '")', $db);
        $id_puzzle = mysql_insert_id();
        $query = dotazSQL(' update hlavolam set je_puzzle="' . $id_puzzle . '" where ID_hlavolam = "' . $id .'"', $db);


    } else if ($type == "riddle") {
        //load riddle related value
        //set flag
        $x = dotazSQL(' insert into hadanka ( druh, jazyk, narocnost_lingvisticka)
                 values ("' . $kind . '","' . $lang . '","' . $langdiff . '")', $db);
        $id_riddle = mysql_insert_id();
        $query = dotazSQL(' update hlavolam set je_hadanka="' . $id_riddle . '" where ID_hlavolam = "' . $id .'"', $db);

    }
    if(mysql_affected_rows() == 1) {
        //mysql_commit($db);
        echo json_encode(array('status' => 'ok'));
    } else {
        //mysql_rollback($db);
        echo json_encode(array('status' => 'error'));
    }

    exit(0);
}

function lend_riddle()
{
    global $db;

    $lender = $_POST['lender'];
    $riddleid = $_POST['riddle'];

    $q = dotazSQL(' select ID_zberatel from zberatel where email="' . $_COOKIE['user'] . '"', $db);
    $res = mysql_fetch_array($q);
    if (mysql_num_rows($q) == 0) {
        die(json_encode(array('status' => 'error', 'errstr' => "Zberatel nieje v databaze")));
    }
    $id = $res[0];

    //Zisti ci hlaovlam je volny
    $q = dotazSQL('select distinct ID_hlavolam from pozicky where ID_hlavolam="'.$riddleid.'"
                  AND (datum_navratu IS NULL OR datum_navratu > NOW()) ', $db);
    if (mysql_num_rows($q) > 0) {
        die(json_encode(array('status' => 'error', 'errstr' => "Hlavolam je pozicany")));
    }
    //If row returned
    $q = dotazSQL('insert into pozicky ( id_vypozicajuci, id_zberatel, id_hlavolam, datum_pozicky )
                      values ("'.$lender.'","'.$id.'","'.$riddleid.'", NOW() )', $db);

    //Ok
    echo json_encode(array('status' => 'ok'));

    exit(0);
}

function get_lenders() {
    global $db;

    $q = dotazSQL('select * from vypozicajuci', $db);
    $data = array();

        while ($row = mysql_fetch_assoc($q)) {
            $data[] = $row;
        }

    echo json_encode($data);
    exit(0);
}

function remove_riddle()
{
    global $db;

    $id = $_POST['riddle'];

    $q = dotazSQL('select je_hadanka, je_puzzle from hlavolam where ID_hlavolam="'.$id.'"', $db);

    $row = array();
    $row = mysql_fetch_assoc($q);

    $q = dotazSQL('SET FOREIGN_KEY_CHECKS=0', $db);

    if ($row["je_hadanka"]) {
        $q = dotazSQL('delete X, H from hlavolam as X inner join hadanka as H ON X.je_hlavolam=H.ID_hadanka where H.ID_hadanka="'.$row["je_hadanka"].'"', $db);
    } else if ($row["je_puzzle"]) {
        $q = dotazSQL('delete X, P from hlavolam as X inner join puzzle as P ON X.je_puzzle=P.ID_puzzle where P.ID_puzzle="'.$row["je_puzzle"].'"', $db);
    } else {
        $q = dotazSQL('delete from hlavolam where ID_hlavolam="' . $id . '"', $db);
    }

    $q = dotazSQL('delete from riesenie where hlavolam="' . $id . '"', $db);
    $q = dotazSQL('delete from pozicky where id_hlavolam="' . $id . '"', $db);

    $q = dotazSQL('SET FOREIGN_KEY_CHECKS=1', $db);


    echo json_encode(array('status' => 'ok'));
    exit();
}


if ($_SERVER["REQUEST_METHOD"] == "POST") {
// pripojenie do db
    $db = prechodneSpojeniSRBD();
    if ($_POST['type'] == 'add-riddle')
        submit_riddle();
    else if ($_POST['type'] == 'lend-riddle')
        lend_riddle();
    else if ($_POST['type'] == 'remove-riddle')
        remove_riddle();
    else if ($_POST['type'] == 'get-lenders')
        get_lenders();

}

?>


<div class="w3-modal" id="riddle-add-modal" hidden>

    <div class="w3-modal-content w3-card-12">
        <header class="w3-container w3-teal w3-padding">
                <span class="w3-closebtn w3-hover-text-black"
                      onclick="$('#riddle-add-modal').hide()">X</span>
            <h3>Nov� hlavolam</h3>
        </header>
        <form class="w3-container" name="form-riddle-add" action="javascript:submit_riddle()" method="POST">
            <div class="w3-section">
                <label><b>N�zov*</b></label>
                <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="" name="name" required>
                <label><b>Typ*</b></label>
                <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="Popis"
                       name="description" required>

                <div class="w3-row-padding w3-margin-bottom ">
                    <div class="w3-third">
                        <input onclick="checkedClass()" class="w3-radio w3-border" type="radio" name="class" id="check-puzzle" value="puzzle">
                        <label class="w3-validate">puzzle</label>
                    </div>
                    <div class="w3-third">
                        <input onclick="checkedClass()" class="w3-radio w3-border" type="radio" name="class" id="check-riddle" value="riddle">
                        <label class="w3-validate">h�danka</label>
                    </div>
                    <div class="w3-third">
                        <input onclick="checkedClass()" class="w3-radio w3-border" type="radio" name="class" id="check-no" value="na" checked>
                        <label class="w3-validate">in�</label>
                    </div>
                </div>

                <div class="w3-section">

                    <div id="form-puzzle" class="w3-row-padding" hidden>
                        <div class="w3-third">
                            <p>Po�et dielov</p>
                            <input class="w3-input w3-border w3-margin-bottom" type="number" placeholder="Po�et"
                                   name="pcs">
                        </div>
                        <div class="w3-third">
                            <p>Autor</p>
                            <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder=""
                                   name="author">
                        </div>
                        <div class="w3-third">
                            <p>T�ma</p>
                            <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder=""
                                   name="theme">
                        </div>
                    </div>

                    <div id="form-riddle" class="w3-row-padding" hidden>
                        <div class="w3-third">
                            <p>Druh</p>
                            <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder=""
                                   name="kind">
                        </div>
                        <div class="w3-third">
                            <p>Jazyk</p>
                            <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder=""
                                   name="lang">
                        </div>
                        <div class="w3-third">
                            <p>Lingvistick� n�ro�nos�</p>
                            <input class="w3-input w3-border w3-margin-bottom" type="number" placeholder="0-100"
                                   name="langdiff" min="0" max="100">
                        </div>
                    </div>
                </div>

                <label><b>Krajina p�vodu</b></label>
                <input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="" name="origin">

                <label><b>Obtia�nos�</b></label>
                <div class="w3-row-padding w3-margin-bottom">
                    <div class="w3-third">
                        <p>Priestorov�</p>
                        <input class="w3-input w3-border w3-margin-bottom" type="number" placeholder="0-100"
                               name="spacediff" min="0" max="100">
                    </div>
                    <div class="w3-third">
                        <p>�asov�</p>
                        <input class="w3-input w3-border w3-margin-bottom" type="number" placeholder="0-100"
                               name="timediff" min="0" max="100">
                    </div>
                    <div class="w3-third">
                        <p>Vedomostn�</p>
                        <input class="w3-input w3-border w3-margin-bottom" type="number" placeholder="0-100"
                               name="minddiff" min="0" max="100">
                    </div>
                </div>


            </div>
            <button class="w3-btn-block w3-green w3-section w3-padding" type="submit">Pridaj</button>
        </form>
        <footer class="w3-container w3-teal">
            <div class="w3-container w3-red" name="submit-riddle-err" hidden>
                <p>Po�iadavka ne�spe�n�, do�lo k chybe</p>
            </div>
            <div class="w3-container w3-green" name="submit-riddle-ok" hidden>
                <p>Po�iadavka bola �spe�n�.</p>
            </div>
        </footer>
    </div>
</div>

<div class="w3-modal" id="riddle-lend-modal" hiddlen>
    <div class="w3-modal-content w3-card-12">

        <header class="w3-container w3-teal w3-padding">
                <span class="w3-closebtn w3-hover-text-black"
                      onclick="$('#riddle-lend-modal').hide(); $('#riddle-lend-modal footer div').hide()">X</span>
            <h3>Zadaj p��i�ku</h3>
        </header>

        <form class="w3-container w3-padding-0" name="form-riddle-lend" action="javascript:lend_riddle()" method="POST">

            <div class="w3-container">
                <p>Id po�i�iavan�ho hlavolamu: </p>
                <p id="lend-id" name="nlend-id">none</p>
                <p>Vypo�i�aj�ci:
                    <select class="w3-select" id="lenders" name="nlenders" required>
                    </select>
                </p>
            </div>
            <div class="btn-group w3-padding-bottom">
                <button class="w3-btn w3-green" id="accept-lend" type="submit">Potvrdi�</button>
                <button class="w3-btn w3-red" id="cancel-lend" type="reset" onclick="$('#riddle-lend-modal').hide(); $('#riddle-lend-modal footer div').hide()">Zavrie�</button>
            </div>
        </form>

            <footer class="w3-container w3-teal w3-padding">

                <div class="w3-container w3-red" name="submit-lend-err" hidden>
                    <p>Po�iadavka ne�spe�n�, hlavolam nieje k dispozicii.</p>
                </div>
                <div class="w3-container w3-green" name="submit-lend-ok" hidden>
                    <p>P��i�ka bola evidovan�.</p>
                </div>
            </footer>

    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript">

    function submit_riddle()
    {
        var data = $("form[name=form-riddle-add]").serializeArray();
        $.post("riddle_actions.php",
            {
                dataType: "json",
                type: "add-riddle",
                name: data[0].value,
                description: data[1].value,
                class: data[2].value,
                pcs: data[3].value,
                author: data[4].value,
                theme: data[5].value,
                kind: data[6].value,
                lang: data[7].value,
                langdiff: data[8].value,
                origin: data[9].value,
                spacediff: data[10].value,
                timediff: data[11].value,
                minddiff: data[12].value

            }).done(function(data) {
                //data = jQuery.parseJSON(data);
                //if (data.status.localeCompare("ok") == 0) {
                    $("div[name=submit-riddle-ok").show();
                    $("div[name=submit-riddle-err").hide();

                    $("form[name=form-riddle-add]").each(function () {
                        this.reset();
                    });
                    checkedClass();
                    fetchRiddles();
                    updateStats();
               // }
            }).fail(function(data) {
                //data = jQuery.parseJSON(data);
                //ne, narev na uzivatela ze co to tam pcha
                $("div[name=submit-riddle-ok").hide();
                $("div[name=submit-riddle-err").show();

            })

    }

    function lend_riddle()
    {
        var data = $("form[name=form-riddle-lend]").serializeArray();
        $.post("riddle_actions.php",
            {
                dataType: "json",
                type: "lend-riddle",
                //from cookies some session token or whatever
                riddle: $("#lend-id").text(),
                lender: data[0].value
            }
            , function(data) {
                data = jQuery.parseJSON(data);
                if (data.status.localeCompare("ok")==0) {
                    //ok, zavri modal
                    $("div[name=submit-lend-ok]").show();
                    $("div[name=submit-lend-err]").hide();
                } else {
                    //ne, narev na uzivatela ze co to tam pcha
                    $("div[name=submit-lend-ok]").hide();
                    $("div[name=submit-lend-err]").show();
                }
            });
    }

    // remove neries, to robim ja
    function remove_riddle(riddleid)
    {
        $.post("riddle_actions.php",
            {
                dataType: "json",
                type: "remove-riddle",
                riddle: riddleid,
                //from cookies some session token or whatever

            }
            , function(data) {
                data = jQuery.parseJSON(data);
                if (data.status.localeCompare("ok")==0) {
                    //ok, zavri modal
                } else {
                    //ne, narev na uzivatela ze co to tam pcha
                }
            });
    }

    function checkedClass() {
        if (document.getElementById('check-riddle').checked) {
            document.getElementById('form-riddle').style.display="block";
            document.getElementById('form-puzzle').style.display="none";
        } else if (document.getElementById('check-puzzle').checked) {
            document.getElementById('form-riddle').style.display="none";
            document.getElementById('form-puzzle').style.display="block";
        } else {
            document.getElementById('form-riddle').style.display="none";
            document.getElementById('form-puzzle').style.display="none";
        }
    }

</script>


