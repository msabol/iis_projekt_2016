
<?php
	// redirect na login pokial nikto nieje prihlaseny
	if(!isset($_COOKIE['user'])) {
		header('Location: login.php');	// odkomentovat az vsetko pojde
	}

	if($_COOKIE['user'] == 'admin') {
		header('Location: admin.php');	// odkomentovat az vsetko pojde
	}

	include "db.php";

	$db = null;

	function logout() {
		// unset cookie
		setcookie("user", "", time()-3600);
		exit();
	}

	function riddles() {
		global $db;

		$user = dotazSQL('select * from zberatel where email="'.$_COOKIE['user'].'"', $db);
		$user = mysql_fetch_assoc($user);
		$riddles = dotazSQL(
			'select * from hlavolam where id_vlastnik="'.$user['ID_zberatel'].'"'
			, $db);
		
		$data = array();
		while ($row = mysql_fetch_assoc($riddles)) {
			   $data[] = $row;
		}

		echo json_encode($data);
		exit();
	}

	function lends() {
		global $db;

		$user = dotazSQL('select * from zberatel where email="'.$_COOKIE['user'].'"', $db);
		$user = mysql_fetch_assoc($user);
		$lends = dotazSQL(
			'select * from pozicky join hlavolam on pozicky.id_hlavolam=hlavolam.ID_hlavolam join vypozicajuci on vypozicajuci.ID_vypozicajuci=pozicky.id_vypozicajuci where id_vlastnik="'.$user['ID_zberatel'].'"'
			, $db);
		
		$data = array();
		while ($row = mysql_fetch_assoc($lends)) {
			   $data[] = $row;
		}

		echo json_encode($data);
		exit();
	}

	function riddle_detail() {
		global $db;
		
		$id = $_POST['id'];
		
		$hlavolamy = dotazSQL(
			"select * from hlavolam left join puzzle on hlavolam.je_puzzle=puzzle.ID_puzzle left join hadanka on hlavolam.je_hadanka=hadanka.ID_hadanka where ID_hlavolam=\"".$id."\"", 
			$db);

		$data = mysql_fetch_assoc($hlavolamy);

		echo json_encode($data);
		exit();
	}

	function riddle_edit() {
		global $db;

		$json = $_POST['new_data'];
		$json = str_replace("\\\"", "\"", $json);	// nutna uprava, ciste by to json_decode neprechrumal

		$data = json_decode($json);

		// update hlavolamu
		dotazSQL("update hlavolam set typ='".$data->typ.
			"', nazov='".$data->nazov.
			"', krajina_povodu='".$data->krajina_povodu.
			"', obtiaznost_priestorova='".$data->obtiaznost_priestorova.
			"', obtiaznost_casova='".$data->obtiaznost_casova.
			"', obtiaznost_vedomostna='".$data->obtiaznost_vedomostna.
			"' where ID_hlavolam=".$data->ID_hlavolam,
			$db);

		// update hadanky
		if($data->ID_hadanka)
			dotazSQL("update hadanka set druh='".$data->druh.
				"', jazyk='".$data->jazyk.
				"', narocnost_lingvisticka='".$data->narocnost_lingvisticka.
				"' where ID_hadanka=".$data->ID_hadanka, 
				$db);

		if($data->ID_puzzle)
			dotazSQL("update puzzle set autor='".$data->autor.
				"', tema='".$data->tema.
				"', pocet_dielov='".$data->pocet_dielov.
				"' where ID_puzzle=".$data->ID_puzzle, 
				$db);

		exit();
	}

if($_SERVER["REQUEST_METHOD"] == "POST") {
		// pripojenie do db
		$db = prechodneSpojeniSRBD();	
		if($_POST['type'] == 'get-riddles')
			riddles();
		else if($_POST['type'] == 'get-lends') 
			lends();
		else if($_POST['type'] == 'logout')
			logout();
		else if($_POST['type'] == 'riddle-detail')
			riddle_detail();
		else if($_POST['type'] == 'riddle-edit')
			riddle_edit();			
}
?>


<!DOCTYPE html>
<html language="sk-SK">
<head>
    <title>Riddle book</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="ISO-8859-2">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" href="w3.css">
</head>
<style>
    .ui {
        padding: 0px;
        display: none;
    }
	table tr.selected {
		background-color: rgba(0, 128, 128, 0.96) !important;
		color:#fff;
		vertical-align: middle;
		padding: 1.5em;
	}
</style>
<body>

<?php include "riddle_actions.php" ?>

<div class="w3-container"> <!--style="margin-left:130px"-->

    <div class="w3-container">
	<div class="w3-container">
		<h2 style="float: left;">Kniha Hlavolamov</h2>
		<h3><button onclick="logout();" class="w3-btn w3-round-large w3-white w3-border w3-border-blue" style="float: right;">Logout</button></h3>
	</div>

	<div class="w3-container">
	    <h2>Aktualne</h2>
	    <div class="w3-progress-container">
	        <div id="solved-riddles-ratio-bar" class="w3-progressbar w3-black" style="width:25%">Vyrie�en�ch hlavolamov</div>
	    </div>
	    <br>
	    <div class="w3-progress-container">
	        <div id="lent-riddles-ratio-bar" class="w3-progressbar w3-teal" style="width:50%">Vypo�i�an�ch hlavolamov</div>
	    </div>
	    <br>

	    <div class="w3-progress-container ">
	        <div id="average-difficult-bar" class="w3-progressbar w3-gray" style="width:75%">Priemern� obtia�nos�</div>
	    </div>
	    <p id="riddles-count-p">loading...</p>
	</div>


        <div class="w3-row toggle-colour">
            <a href="javascript:void(0)" onclick="openUi(event, 'Riddles');">
                <div class="w3-third tablink w3-bottombar w3-hover-light-grey w3-padding w3-border-teal">Hlavolamy</div>
            </a>
            <a href="javascript:void(0)" onclick="openUi(event, 'Lend');">
                <div class="w3-third tablink w3-bottombar w3-hover-light-grey w3-padding">P��i�ky</div>                
            </a>

        </div>

        <div id="Riddles" class="w3-container ui" style="display:block">
            <div class="w3-row w3-padding-4">
                <input class="w3-threequarter" id="filter" type="text" class="w3-input" placeholder="Search.."
                       onkeyup="filterContent('Riddles')">
                <button onclick="fetchRiddles();updateStats()" class="w3-btn w3-quarter">Reload</button>
            </div>
			<div class="w3-row w3-padding-4">

				<div class=" w3-btn-bar w3-border">

					<button class="w3-btn" onclick="document.getElementById('riddle-add-modal').style.display='block'">
						Nov�
					</button>
					<button class="w3-btn" onclick="riddleDetail($('#Riddles .selected').attr('key'))">Detail</button>
					<button class="w3-btn" onclick="riddleEdit($('#Riddles .selected').attr('key')),
						$('#Riddles .selected').rowIndex">Upravi�
					</button>


					<button class="w3-btn" onclick="solutionsShow($('#Riddles .selected').attr('key'))">Zobrazit
						rie�enie
					</button>

					<button class="w3-btn" onclick="catchRiddleId($('#Riddles .selected').attr('key'));
				 		$('#riddle-lend-modal').show()">Po�i�a�
					</button>
					<button class="w3-btn" onclick="addLeaser($('#Riddles .selected').attr('key'))">Prida�
						vypo�i�aj�ceho
					</button>

					<button class="w3-btn" onclick="remove_riddle($('#Riddles .selected').attr('key'));
                		$('#Riddles .selected').remove()">Zmaza�
					</button>
				</div>
			</div>

            <table class="w3-table-all w3-hoverable" name="riddles">
                <thead>
                <tr class="w3-light-grey">
                    <th>N�zov</th>
                    <th>Krajina p�vodu</th>
                    <th>Puzzle?</th>
                    <th>H�danka?</th>
                    <th>Obtia�nos�</th>
                </tr>
                </thead>
		<tbody>
		</tbody>
            </table>


        </div>
        <div id="Lend" class="w3-container ui">
            <div class="w3-row w3-padding-4">
                <input class="w3-threequarter" id="filter" type="text" class="w3-input" placeholder="Search.."
                       onkeyup="filterContent('Lend')">
                <button onclick="fetchLends();updateStats()" class="w3-btn w3-quarter">Reload</button>
            </div>

            <div class="w3-btn-bar w3-border">
                <button class="w3-btn" onclick="lendReturn($('#Lend .selected').attr('key'))">Vr�</button>
                <button class="w3-btn" onclick="lendDetail($('#Lend .selected').attr('key'))">Detail</button>
                <button class="w3-btn" onclick="lendEdit($('#Lend .selected').attr('key'))">Upravi�</button>
            </div>

            <table class="w3-table-all w3-hoverable" name="lends">
                <thead>
		<tr class="w3-light-grey">
                    <th>N�zov hlavolamu</th>
                    <th>D�tum p��i�ky</th>
                    <th>D�tum n�vratu</th>
                    <th>Vypo�i�aj�ci</th>
                    <th>�spe�ne vyrie�en�</th>
                </tr>
                </thead>
                <tbody>
                </tbody>
            </table>
        </div>
    </div>

</div>

<div class="w3-modal" id="riddle-detail-modal" style="display:none">
    <div class="w3-modal-content w3-card-12">
        <header class="w3-container w3-teal w3-padding">
            <span class="w3-closebtn w3-hover-text-black"
                      onclick="document.getElementById('riddle-detail-modal').style.display='none'">X</span>
            <h3>Detail hlavolamu</h3>
	</header>
	<div class="w3-container">
		<div id="riddle-main-info">
			<h5>N�zov:</h5>
			<p id="riddle-nazov"></p><br>
			<h5>Typ:</h5>
			<p id="riddle-typ"></p><br>
			<h5>Krajina p�vodu:</h5>
			<p id="riddle-krajina-povodu"></p><br>
			<h5>Priestorov� obtia�nos�:</h5>
			<p id="riddle-obtiaznost-priestorova"></p><br>
			<h5>�asov� obtia�nos�:</h5>
			<p id="riddle-obtiaznost-casova"></p><br>
			<h5>Vedomostn� obtia�nos�:</h5>
			<p id="riddle-obtiaznost-vedomostna"></p><br>
		</div>
		<div id="riddle-puzzle-info" hidden>
			<h5>Autor:</h5>
			<p id="riddle-puzzle-autor"></p><br>
			<h5>T�ma:</h5>
			<p id="riddle-puzzle-tema"></p><br>
			<h5>Po�et dielov:</h5>
			<p id="riddle-puzzle-pocet-dielov"></p><br>
		</div>
		<div id="riddle-hadanka-info" hidden>
			<h5>Druh h�danky:</h5>
			<p id="riddle-hadanka-druh"></p><br>
			<h5>Jazyk h�danky:</h5>
			<p id="riddle-hadanka-jazyk"></p><br>
			<h5>Lingvistick� n�ro�nos�:</h5>
			<p id="riddle-hadanka-narocnost-lingvisticka"></p><br>
		</div>
	</div>

    </div>
</div>

<div class="w3-modal" id="riddle-edit-modal" style="display:none">
    <div class="w3-modal-content w3-card-12">
        <header class="w3-container w3-teal w3-padding">
            <span class="w3-closebtn w3-hover-text-black"
                      onclick="document.getElementById('riddle-edit-modal').style.display='none'">X</span>
            <h3>�prava hlavolamu</h3>
	</header>
	<div class="w3-container">
		<div id="riddle-main-edit">
			<h5>N�zov:</h5>
			<input type="text" id="edit-nazov"><br>
			<h5>Typ:</h5>
			<input type="text" id="edit-typ"><br>
			<h5>Krajina p�vodu:</h5>
			<input type="text" id="edit-krajina-povodu"><br>
			<h5>Priestorov� obtia�nos�:</h5>
			<input type="text" placeholder="0-100" id="edit-obtiaznost-priestorova"><br>
			<h5>�asov� obtia�nos�:</h5>
			<input type="text" placeholder="0-100" id="edit-obtiaznost-casova"><br>
			<h5>Vedomostn� obtia�nos�:</h5>
			<input type="text" placeholder="0-100" id="edit-obtiaznost-vedomostna"><br>
		</div>
		<div id="riddle-puzzle-edit" hidden>
			<h5>Autor:</h5>
			<input type="text" id="edit-puzzle-autor"><br>
			<h5>T�ma:</h5>
			<input type="text" id="edit-puzzle-tema"><br>
			<h5>Po�et dielov:</h5>
			<input type="text" id="edit-puzzle-pocet-dielov"><br>
		</div>
		<div id="riddle-hadanka-edit" hidden>
			<h5>Druh h�danky:</h5>
			<input type="text" id="edit-hadanka-druh"><br>
			<h5>Jazyk h�danky:</h5>
			<input type="text" id="edit-hadanka-jazyk"><br>
			<h5>Lingvistick� n�ro�nos�:</h5>
			<input type="text" placeholder="0-100" id="edit-hadanka-narocnost-lingvisticka"><br>
		</div>
	</div>
	<footer class="w3-container w3-teal w3-padding">
		<div class="btn-group">
			<button class="w3-btn w3-green" id="accept-edit">Upravi�</button>
			<button class="w3-btn w3-red" id="cancel-edit" onclick="document.getElementById('riddle-edit-modal').style.display='none';">Zru�i�</button>
		</div>
	</footer>
    </div>
</div>



<?php	include "lends.php" ?>
<?php	include "stats.php" ?>
<?php	include "solutions.php" ?>

<script>

	$("tbody").on('click', 'tr', function (event)
	{
		$(".selected").removeClass('selected');
		$(this).addClass('selected');
	});


	$(document).ready( fetchAll() );

	function fetchAll() {
		// pouzivatelove hadanky
		fetchRiddles();
		fetchLends();
		fetchLenders();

		updateStats();
	}

	function updateStats() {
		$.post("stats.php",
			{
				type: "get-solved-riddles-ratio",
				dataType: "json"
			},
			function(data) {
				data = jQuery.parseJSON(data);
				var elem = document.getElementById("solved-riddles-ratio-bar");
				elem.style.width = data + '%';
			});

		$.post("stats.php",
			{
				type: "get-lent-riddles-ratio",
				dataType: "json"
			},
			function(data) {
				data = jQuery.parseJSON(data);
				var elem = document.getElementById("lent-riddles-ratio-bar");
				elem.style.width = data + '%';
			});

		$.post("stats.php",
			{
				type: "get-average-difficult",
				dataType: "json"
			},
			function(data) {
				data = jQuery.parseJSON(data);
				var elem = document.getElementById("average-difficult-bar");
				elem.style.width = data + '%';
			});

		$.post("stats.php",
			{
				type: "get-riddles-count",
				dataType: "json"
			},
			function(data) {
				data = jQuery.parseJSON(data);
				$("p#riddles-count-p").text('Po�et vypo�i�an�ch hlavolamov: '+data);
			});

	}

	function fetchRiddles() {
		$.post("index.php",
			{
				type: "get-riddles",
				dataType: "json"
			},
			function(data) {
				data = jQuery.parseJSON(data);
				$('table[name=riddles] tbody').empty();
				for(var i = 0; i < data.length; ++i) {
					var row = "<tr key=\""+data[i].ID_hlavolam+"\" class=\"riddlesRow"+i+"\">";

					row += "<td >"+ data[i].nazov +"</td>";
					row += "<td>"+ (data[i].krajina_povodu ? data[i].krajina_povodu : "???")+"</td>";
					row += "<td>"+ (data[i].je_puzzle ? "V" : "X") +"</td>";
					row += "<td>"+ (data[i].je_hadanka ? "V" : "X") +"</td>";
					var priemerna_obtiaznost = (parseInt(data[i].obtiaznost_priestorova) + parseInt(data[i].obtiaznost_casova) + parseInt(data[i].obtiaznost_vedomostna) )/3.0;
					row += "<td>"+ (priemerna_obtiaznost).toString() +"</td>";


					$("table[name=riddles] > tbody").append(row);
				}
			});
	}

	function fetchLends() {
		$.post("index.php",
			{
				type: "get-lends",
				dataType: "json"
			},
			function(data) {
				$('table[name=lends] tbody').empty();
				data = jQuery.parseJSON(data);
				for(var i = 0; i < data.length; ++i) {
					var row = "<tr key=\""+data[i].ID_pozicka+"\" >";

					row += "<td>"+ data[i].nazov +"</td>";
					row += "<td>"+ data[i].datum_pozicky +"</td>";
					row += "<td>"+ (data[i].datum_navratu ? data[i].datum_navratu : "-") +"</td>";
					row += "<td>"+ (data[i].id_vypozicajuci ? data[i].meno: data[i].jmeno) +"</td>";
					row += "<td>"+ (data[i].uspesne_vyriesene == "y" ? "V" : "X") +"</td>";


					$("table[name=lends] > tbody").append(row);
				}
			});
	}

	function fetchLenders() {
		$.post("riddle_actions.php",
			{
				type: "get-lenders",
				dataType: "json"
			},
			function (data) {
				data = jQuery.parseJSON(data);
				var options = "";
				for (var i = 0; i < data.length; ++i) {
					var row = "<option value=\"" + data[i].ID_vypozicajuci + "\">" + data[i].meno + "</option>\n";
					options += row;
				}
				$("#lenders").empty().append(options);
			});
	}

	function logout() {
		$.post("index.php",
		{
			type: "logout"
		},
		function(data) {
			window.location.href = "login.php"
		});
	}

	function riddleDetail(riddle_id) {
		$.post("index.php",
       		{
			type: "riddle-detail",
			id: riddle_id,
			dataType: "json"
		}, 
		function(data) {
			data = jQuery.parseJSON(data);
			// znovu skry extra info
			$("div#riddle-puzzle-info").hide();
			$("div#riddle-hadanka-info").hide();

			// zakladne info
			$("p#riddle-nazov").text(data.nazov);
			$("p#riddle-typ").text(data.typ);
			$("p#riddle-krajina-povodu").text(data.krajina_povodu);
			$("p#riddle-obtiaznost-priestorova").text(data.obtiaznost_priestorova);
			$("p#riddle-obtiaznost-casova").text(data.obtiaznost_casova);
			$("p#riddle-obtiaznost-vedomostna").text(data.obtiaznost_vedomostna);

			// puzzle info
			if(data.je_puzzle) {
				$("p#riddle-puzzle-autor").text(data.autor);
				$("p#riddle-puzzle-tema").text(data.tema);
				$("p#riddle-puzzle-pocet-dielov").text(data.pocet_dielov);
				$("div#riddle-puzzle-info").show();
			}

			// hadanka info
			if(data.je_hadanka) {
				$("p#riddle-hadanka-druh").text(data.druh);
				$("p#riddle-hadanka-jazyk").text(data.jazyk);
				$("p#riddle-hadanka-narocnost-lingvisticka").text(data.narocnost_lingvisticka);
				$("div#riddle-hadanka-info").show();
			}

			// display modal
			document.getElementById("riddle-detail-modal").style.display='block';
		});
	}

	function riddleEdit(riddle_id, riddle_row) {
		$.post("index.php",
       		{
			type: "riddle-detail",
			id: riddle_id,
			dataType: "json"
		}, 
		function(data) {
			data = jQuery.parseJSON(data);
			// znovu skry extra info
			$("div#riddle-puzzle-edit").hide();
			$("div#riddle-hadanka-edit").hide();

			// zakladne info
			$("input#edit-nazov").val(data.nazov);
			$("input#edit-typ").val(data.typ);
			$("input#edit-krajina-povodu").val(data.krajina_povodu);
			$("input#edit-obtiaznost-priestorova").val(data.obtiaznost_priestorova);
			$("input#edit-obtiaznost-casova").val(data.obtiaznost_casova);
			$("input#edit-obtiaznost-vedomostna").val(data.obtiaznost_vedomostna);

			// puzzle info
			if(data.je_puzzle) {
				$("input#edit-puzzle-autor").val(data.autor);
				$("input#edit-puzzle-tema").val(data.tema);
				$("input#edit-puzzle-pocet-dielov").val(data.pocet_dielov);
				$("div#riddle-puzzle-edit").show();
			}

			// hadanka info
			if(data.je_hadanka) {
				$("input#edit-hadanka-druh").val(data.druh);
				$("input#edit-hadanka-jazyk").val(data.jazyk);
				$("input#edit-hadanka-narocnost-lingvisticka").val(data.narocnost_lingvisticka);
				$("div#riddle-hadanka-edit").show();
			}

			// nastav funkciu upravenia
			$("button#accept-edit").click(function() {
				// zober vsetky data spat do data a zakoduj do JSON
				data.nazov = $("input#edit-nazov").val();
				data.typ = $("input#edit-typ").val();
				data.krajina_povodu = $("input#edit-krajina-povodu").val();

				data.obtiaznost_priestorova = $("input#edit-obtiaznost-priestorova").val();
				if(data.obtiaznost_priestorova < 0) data.obtiaznost_priestorova = 0;
				else if(data.obtiaznost_priestorova > 100) data.obtiaznost_priestorova = 100;

				data.obtiaznost_casova = $("input#edit-obtiaznost-casova").val();
				if(data.obtiaznost_casova < 0) data.obtiaznost_casova = 0;
				else if(data.obtiaznost_casova > 100) data.obtiaznost_casova = 100;

				data.obtiaznost_vedomostna = $("input#edit-obtiaznost-vedomostna").val();
				if(data.obtiaznost_vedomostna < 0) data.obtiaznost_vedomostna = 0;
				else if(data.obtiaznost_vedomostna > 100) data.obtiaznost_vedomostna = 100;

				if(data.je_puzzle) {
					data.autor = $("input#edit-puzzle-autor").val();
					data.tema = $("input#edit-puzzle-tema").val();
					data.pocet_dielov = $("input#edit-puzzle-pocet-dielov").val();
				}
				if(data.je_hadanka) {
					data.druh = $("input#edit-hadanka-druh").val();
					data.jazyk = $("input#edit-hadanka-jazyk").val();
					data.narocnost_lingvisticka = $("input#edit-hadanka-narocnost-lingvisticka").val();
				}

				var json_data = JSON.stringify(data);

				$.post("index.php",
				{
					type: 'riddle-edit',
					dataType: 'json',
					contentType: "application/json",
					new_data: json_data
				},
				function(){
					// zavri modal	
					document.getElementById("riddle-edit-modal").style.display='none';

					// refresh stranky
					document.location.href = "index.php";
				});
			});

			// display modal
			document.getElementById("riddle-edit-modal").style.display='block';
		});
	}

	function catchRiddleId(riddle_id) {
		$("#lend-id").text(riddle_id);
		$("#riddle-lend-modal").show();
	}

    function openUi(evt, ui) {
        var i, x, tablinks;
        x = document.getElementsByClassName("ui");
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("toggle-colour")[0].getElementsByClassName("tablink");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" w3-border-teal", "");
        }
        document.getElementById(ui).style.display = "block";
        evt.currentTarget.firstElementChild.className += " w3-border-teal";
    }

    function filterContent(tableId) {
		var input, filter, table, tr, td, i;
		input = document.getElementById(tableId).getElementsByTagName("input")[0];
		filter = input.value.toUpperCase();
		table = document.getElementById(tableId).getElementsByTagName("table")[0];
		tr = table.getElementsByTagName("tr");
		for (i = 0; i < tr.length; i++) {
			td = tr[i].getElementsByTagName("td")[0];
			if (td) {
				if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
					tr[i].style.display = "";
				} else {
					tr[i].style.display = "none";
				}
			}
		}
	}



</script>

</body>
</html>

