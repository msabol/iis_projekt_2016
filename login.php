<?php

if(isset($_COOKIE['user']) && $_COOKIE['user'] == 'admin') {
	header("Location: admin.php");
} else if(isset($_COOKIE['user'])) {
	header("Location: index.php");
}

include 'db.php';

$db = null;

// REGISTRACIA
function register() {
	global $db;

	$name = $_POST['name'];
	$email = $_POST['email'];
	$password = $_POST['password'];

	$query = dotazSQL('select * from zberatel where email="'.$email.'"', $db);
	if(mysql_num_rows($query) > 0 || $email == 'admin' ) {
		echo json_encode(array('status' => 'error'));
		exit();
	}

	// vlozit do db
	$query = dotazSQL('insert into zberatel (jmeno, email, heslo) values (\''.$name.'\', \''.$email.'\', \''.crypt($password, $email).'\')', $db);
	echo json_encode(array('status' => 'ok'));
	exit();
}

// LOGIN
function login() {
	global $db;

	$email = $_POST['email'];
	$password = $_POST['password'];

	if($email == 'admin' && $password == 'admin') {
		setcookie('user', $email);
		echo json_encode(array('status' => 'ok'));
		exit();
	}

	$query = dotazSQL('select * from zberatel where email="'.$email.'" and heslo="'.crypt($password, $email).'"', $db);
	if(mysql_num_rows($query) == 0) {
		echo json_encode(array('status' => 'error'));
		exit();
	} else {
		// pozri ci je aktivny
		$user = mysql_fetch_assoc($query);
		if($user['aktivny'] == 'n') {
			echo json_encode(array('status' => 'inactive'));
			exit();
		}

		// login uspesny, nastav cookie a posli ok
		setcookie('user', $email);
		echo json_encode(array('status' => 'ok'));
		exit();
	}
}

if($_SERVER["REQUEST_METHOD"] == "POST") {
	// pripojenie do db
	$db = prechodneSpojeniSRBD();	
	if($_POST['type'] == 'register')
		register();
	else if($_POST['type'] == 'login')
		login();
}

?>

<!DOCTYPE html>
<html language="sk-SK">
<head>
    <meta charset="ISO-8859-2">
	<title>Riddle book - login | register</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
	<link rel="stylesheet" href="w3.css">
</head>
<style>
    .ui {
        padding: 0px;
        display: none;
    }
</style>
<body>
	<div class="w3-center w3-row">
		<h1>Riddle book</h1>
		<div class="w3-col s6">
			<div class="w3-container w3-red" name="login-msg-err" hidden>
				<p>Nespr�vny prihlasovac� email alebo heslo.</p>
			</div>
			<div class="w3-container w3-red" name="login-msg-inactive" hidden>
				<p>��et nie je akt�vny.</p>
			</div>
			<h3>Prihl�senie</h3>
			<form class="w3-container" name="form-login" action="javascript:ajaxLogin()" method="POST">
				<div class="w3-section">
					<label><b>Email</b></label>
					<input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="" name="username" required>
					<label><b>Heslo</b></label>
					<input class="w3-input w3-border" type="password" placeholder="" name="password" required>
					<button class="w3-btn-block w3-green w3-section w3-padding" type="submit">Login</button>
				</div>
			</form>
		</div>

		<div class="w3-col s6">
			<div class="w3-container w3-green" name="register-msg-ok" hidden>
				<p>Registr�cia prebehla �spe�ne. Pre prihl�senie me potom po�ka� na aktiv�ciu ��tu, ktor� prebehe do 24 hod�n.</p>
			</div>
			<div class="w3-container w3-red" name="register-msg-err" hidden>
				<p>Registr�cia ne�spe�n�. Pou��vate� s t�mto emailom u� existuje.</p>
			</div>
			<h3>Registr�cia</h3>
			<form class="w3-container" name="form-register" action="javascript:ajaxRegister()" method="POST">
				<div class="w3-section">
					<label><b>Meno</b></label>
					<input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="" name="name" required>
					<label><b>Email</b></label>
					<input class="w3-input w3-border w3-margin-bottom" type="text" placeholder="" name="username" required>
					<label><b>Heslo</b></label>
					<input class="w3-input w3-border" type="password" placeholder="" name="password" required>
					<button name="btn-register" class="w3-btn-block w3-green w3-section w3-padding" type="submit">Registrova�</button>
				</div>
			</form>
        </div>

</body>
</html>




<script type="text/javascript">

function ajaxLogin() {
	var data = $("form[name=form-login]").serializeArray();
	$.post("login.php",
	{
		dataType: "json",
		email: data[0].value,
		password: data[1].value,
		type: "login"
	},
	function(data) {
		data = jQuery.parseJSON(data);	
		if(data.status.localeCompare("ok") == 0) {
			// OK, redirect na index.php
			window.location.href = "index.php";
		} else if (data.status.localeCompare("inactive") == 0) {
			$("div[name=login-msg-inactive]").show();
		} else {
			// zle, ukaz spravu
			$("div[name=login-msg-err]").show();
		}
	});
}

function ajaxRegister() {
	var data = $("form[name=form-register]").serializeArray();
	$.post("login.php",
	{
		dataType: "json",
		name: data[0].value,
		email: data[1].value,
		password: data[2].value,
		type: "register"
	},
	function(data) {
		data = jQuery.parseJSON(data);	
		if(data.status.localeCompare("ok") == 0) {
			// OK, ukaz spravu
			$("div[name=register-msg-ok]").show();
			$("div[name=register-msg-err]").hide();

			$("form[name=form-register]").each(function() {
				this.reset();
			});
		} else {
			// zle, ukaz spravu
			$("div[name=register-msg-err]").show();
			$("div[name=register-msg-ok]").hide();
		}
	});
}

</script>

