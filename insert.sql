
-- INSERT INTO zberatel (ID_zberatel, jmeno, email, heslo) 
-- VALUES('001', 'Arnold Schwarzenegger', 'arnold@muscles.com', 'NoPainNoGain');

INSERT INTO puzzle (ID_puzzle, pocet_dielov, autor, tema) 
VALUES('001', '24', 'Dino', 'Kreslene postavy');

-- INSERT INTO hadanka (ID_hadanka) 
-- VALUES('000');
INSERT INTO hadanka (ID_hadanka, druh, jazyk) 
VALUES('001', 'logicka', 'CZ');

INSERT INTO hlavolam (ID_hlavolam, typ, nazov, krajina_povodu, je_puzzle, je_hadanka, obtiaznost_priestorova, obtiaznost_casova, obtiaznost_vedomostna, id_vlastnik) 
VALUES('001', 'plastovy', 'Rubikova kostka', 'H', null, null, '00', '30', '00', '001');
INSERT INTO hlavolam (ID_hlavolam, typ, nazov, krajina_povodu, je_puzzle, je_hadanka, obtiaznost_priestorova, obtiaznost_casova, obtiaznost_vedomostna, id_vlastnik) 
VALUES('002', 'puzzle', 'Mickey Mouse', 'SK', '001', null, '60', '00', '00', '001');
INSERT INTO hlavolam (ID_hlavolam, typ, nazov, krajina_povodu, je_puzzle, je_hadanka, obtiaznost_priestorova, obtiaznost_casova, obtiaznost_vedomostna, id_vlastnik) 
VALUES('003', 'hadanka', 'Topinky', 'CZ', null, '001', '00', '30', '00', '001');

INSERT INTO vypozicajuci (ID_vypozicajuci, meno) 
VALUES('001', 'Delia Ellis');
INSERT INTO vypozicajuci (ID_vypozicajuci, meno) 
VALUES('002', 'Pat Potter');
INSERT INTO vypozicajuci (ID_vypozicajuci, meno) 
VALUES('003', 'Dominick Stokes');
INSERT INTO vypozicajuci (ID_vypozicajuci, meno) 
VALUES('004', 'Muriel Foster');

INSERT INTO riesenie (id_riesenie, hlavolam, vypozicajuci, popis_riesenia, najlepsi_cas) 
VALUES('001', '001', '001', 'Bylo potreba 8 otoceni.', '53');
INSERT INTO riesenie (id_riesenie, hlavolam, vypozicajuci, popis_riesenia, najlepsi_cas) 
VALUES('002', '002', '001', 'Slozeno.', '47');
INSERT INTO riesenie (id_riesenie, hlavolam, vypozicajuci, popis_riesenia, najlepsi_cas) 
VALUES('003', '001', '002', 'Bylo potreba 12 otoceni.', '179');
INSERT INTO riesenie (id_riesenie, hlavolam, vypozicajuci, popis_riesenia, najlepsi_cas) 
VALUES('004', '003', '003', 'Za 15 minut. Po 5ti minutach 1 sundate a 2. otocite, po 10ti minutach je 1 hotova a 2 je treba dosmazit jeste z 1 strany.', '411');
INSERT INTO riesenie (id_riesenie, hlavolam, vypozicajuci, popis_riesenia, najlepsi_cas) 
VALUES('005', '003', '004', '15 minut', '85');

INSERT INTO pozicky (ID_pozicka, id_vypozicajuci, id_zberatel, datum_pozicky, datum_navratu, uspesne_vyriesene, id_hlavolam) 
VALUES('001', '001', '001', STR_TO_DATE('2003/05/03 10:02:44', '%Y/%m/%d %H:%i:%s'), STR_TO_DATE('2003/05/28 12:49:22', '%Y/%m/%d %H:%i:%s'), 'y', '001');
INSERT INTO pozicky (ID_pozicka, id_vypozicajuci, id_zberatel, datum_pozicky, datum_navratu, uspesne_vyriesene, id_hlavolam) 
VALUES('002', '002', '001', STR_TO_DATE('2003/05/08 8:15:32', '%Y/%m/%d %H:%i:%s'), STR_TO_DATE('2003/06/01 12:49:22', '%Y/%m/%d %H:%i:%s'), 'y', '002');
INSERT INTO pozicky (ID_pozicka, id_vypozicajuci, id_zberatel, datum_pozicky, datum_navratu, uspesne_vyriesene, id_hlavolam) 
VALUES('003', '003', '001', STR_TO_DATE('2003/05/12 11:16:48', '%Y/%m/%d %H:%i:%s'), STR_TO_DATE('2003/06/01 12:49:22', '%Y/%m/%d %H:%i:%s'), 'y', '001');
INSERT INTO pozicky (ID_pozicka, id_vypozicajuci, id_zberatel, datum_pozicky, datum_navratu, uspesne_vyriesene, id_hlavolam) 
VALUES('004', '001', '001', STR_TO_DATE('2003/06/01 8:31:05', '%Y/%m/%d %H:%i:%s'), STR_TO_DATE('2001/08/01 12:49:22', '%Y/%m/%d %H:%i:%s'), 'n', '001');
INSERT INTO pozicky (ID_pozicka, id_vypozicajuci, id_zberatel, datum_pozicky, datum_navratu, uspesne_vyriesene, id_hlavolam) 
VALUES('005', '003', '001', STR_TO_DATE('2003/06/03 9:08:02', '%Y/%m/%d %H:%i:%s'), STR_TO_DATE('2003/09/01 12:49:22', '%Y/%m/%d %H:%i:%s'), 'n', '003');
INSERT INTO pozicky (ID_pozicka, id_vypozicajuci, id_zberatel, datum_pozicky, datum_navratu, uspesne_vyriesene, id_hlavolam) 
VALUES('006', '001', '001', STR_TO_DATE('2003/07/05 16:51:12', '%Y/%m/%d %H:%i:%s'), STR_TO_DATE('2003/09/01 12:49:22', '%Y/%m/%d %H:%i:%s'), 'y', '003');
INSERT INTO pozicky (ID_pozicka, id_vypozicajuci, id_zberatel, datum_pozicky, datum_navratu, uspesne_vyriesene, id_hlavolam) 
VALUES('007', '004', '001', STR_TO_DATE('2003/08/20 14:03:35', '%Y/%m/%d %H:%i:%s'), STR_TO_DATE('2003/09/01 12:49:22', '%Y/%m/%d %H:%i:%s'), 'y', '003');


