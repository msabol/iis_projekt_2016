<?php

	require_once "db.php";

	$db = null;

	function submit_solution() {
		global $db;

		$riddle = $_POST['riddle'];
		$solver = $_POST['nsolvers'];
		$description = $_POST['ndescription'];
		$besttime = $_POST['nbesttime'];

		//var_dump($_POST);

	//	INSERT INTO riesenie (id_riesenie, hlavolam, vypozicajuci, popis_riesenia, najlepsi_cas)
	//	VALUES('001', '001', '001', 'Bylo potreba 8 otoceni.', '53');

		dotazSQL(' insert into riesenie (hlavolam, vypozicajuci, popis_riesenia, najlepsi_cas)
					  values ("' . $riddle . '","' . $solver . '","' . $description . '","' . $besttime . '")', $db);

		exit();
	}

	function solution_detail() {
		global $db;
		
		$id = $_POST['id'];
		
		$riesenie = dotazSQL(
			"select * from riesenie join vypozicajuci on vypozicajuci.ID_vypozicajuci=riesenie.vypozicajuci where ID_riesenie=\"".$id."\"", 
			$db);

		$data = mysql_fetch_assoc($riesenie);

		echo json_encode($data);
		exit();
	}

	function solutions() {
		global $db;

		$id = $_POST['id'];
		
		$solutions = dotazSQL(
			"select * from riesenie join hlavolam on riesenie.hlavolam=hlavolam.ID_hlavolam join vypozicajuci on vypozicajuci.ID_vypozicajuci=riesenie.vypozicajuci where ID_hlavolam=\"".$id."\"", 
			$db);

		$data = array();
		while ($row = mysql_fetch_assoc($solutions)) {
			   $data[] = $row;
		}

		echo json_encode($data);
		exit();
	}

	function get_all_lenders() {
		global $db;

		$q = dotazSQL('select * from vypozicajuci', $db);
		$data = array();

		while ($row = mysql_fetch_assoc($q)) {
			$data[] = $row;
		}

		echo json_encode($data);
		exit();
	}

if($_SERVER["REQUEST_METHOD"] == "POST") {
		// pripojenie do db
		$db = prechodneSpojeniSRBD();	
		if($_POST['type'] == 'get-solutions')
			solutions();
		else if($_POST['type'] == 'solution-detail')
			solution_detail();
		else if ($_POST['type'] == 'get-all-lenders')
			get_all_lenders();
		else if ($_POST['type'] == 'submit-solution')
			submit_solution();
}

?>

<div class="w3-modal" id="solution-add-modal" style="display:none">

	<div class="w3-modal-content w3-card-12">
		<header class="w3-container w3-teal w3-padding">
				<span class="w3-closebtn w3-hover-text-black"
					  onclick="$('#solution-add-modal').hide()">X</span>
			<h3>Nov� rie�enie</h3>
		</header>
		<form class="w3-container" name="form-solution-add" action="javascript:submitSolution()" method="POST">
			<div class="w3-section">
				<label><b>Rie�ite�</b></label>
				<select class="w3-select" id="solvers" name="nsolvers" required>
				</select>
				<label><b>Popis rie�enie</b></label>
				<input class="w3-input w3-border w3-margin-bottom" type="text" name="ndescription" required>

				<label><b>Najlep�� �as</b></label>
				<input class="w3-input w3-border w3-margin-bottom" type="number" name="nbesttime" min="0">
		
			</div>
			<button class="w3-btn-block w3-green w3-section w3-padding" type="submit">Pridaj</button>
		</form>
	</div>
</div>

<div class="w3-modal" id="solution-modal" style="display:none">
	<div class="w3-modal-content w3-card-12">
		<header class="w3-container w3-teal w3-padding">
			<span class="w3-closebtn w3-hover-text-black"
				onclick="document.getElementById('solution-modal').style.display='none'">X</span>
			<h3 id="riddle-solution"></h3>
	</header>
	<div id="Solution" class="w3-container">
		<div class="w3-btn-bar w3-border">
			<button class="w3-btn" onclick="solutionAdd($('table[name=solutions] tbody').attr('riddleid'))">Pridaj</button>
			<button class="w3-btn" onclick="solutionDetail($('#Solution .selected').attr('key'))">Detail</button>
		</div>

		<table class="w3-table-all w3-hoverable" name="solutions">
			<thead>
			<tr class="w3-light-grey">
				<th>Rie�ite�</th>
				<th>Popis rie�enie</th>
				<th>Najlep�� �as</th>
			</tr>
			</thead>
			<tbody>
			</tbody>
		</table>
	</div>

	</div>
</div>



<div class="w3-modal" id="solution-detail-modal" style="display:none">
	<div class="w3-modal-content w3-card-12">
		<header class="w3-container w3-teal w3-padding">
			<span class="w3-closebtn w3-hover-text-black"
				onclick="document.getElementById('solution-detail-modal').style.display='none'">X</span>
			<h3>Detail rie�enie</h3>
	</header>
	<div class="w3-container">
		<div id="solution-main-info">
			<h5>Rie�ite�:</h5>
			<p id="solution-resitel"></p><br>
			<h5>Popis rie�enie:</h5>
			<p id="solution-popis"></p><br>
			<h5>Najlep�� �as:</h5>
			<p id="solution-nejlepsi-cas"></p><br>
		</div>
	</div>

	</div>
</div>




<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript">

function solutionAdd()
	{
		//document.getElementById("solution-add-modal").style.display='block';

		$.post("solutions.php",
			{
			type: "get-all-lenders",
			dataType: "json"
		}, 
		function(data) {
			data = jQuery.parseJSON(data);

			var options = "";
			for (var i = 0; i < data.length; ++i) {
				var row = "<option value=\"" + data[i].ID_vypozicajuci + "\">" + data[i].meno + "</option>\n";
				options += row;
			}
			$("#solvers").empty().append(options);

			document.getElementById("solution-add-modal").style.display='block';
			$("#solution-modal").hide();
		});
		
	}

function submitSolution()
	{
		var data = $("form[name=form-solution-add]").serializeArray();
		$.post("solutions.php",
			{
				dataType: "json",
				type: "submit-solution",
				riddle: $('table[name=solutions] tbody').attr("riddleid"),		// TADY JE TO POTREBA
				nsolvers: data[0].value,
				ndescription: data[1].value,
				nbesttime: data[2].value

			},
			function(data) {
/*				data = jQuery.parseJSON(data);
				var options = "";
				for (var i = 0; i < data.length; ++i) {
					var row = "<option value=\"" + data[i].ID_vypozicajuci + "\">" + data[i].meno + "</option>\n";
					options += row;
				}
				$("#solvers").empty().append(options);
*/
				document.getElementById("solution-add-modal").style.display='none';
			});

	}

function solutionDetail(solution_id) {
		$.post("solutions.php",
			{
			type: "solution-detail",
			id: solution_id,
			dataType: "json"
		}, 
		function(data) {
			data = jQuery.parseJSON(data);

			// zakladne info
			$("p#solution-resitel").text(data.meno);
			$("p#solution-popis").text(data.popis_riesenia);
			$("p#solution-nejlepsi-cas").text(data.najlepsi_cas);

			// display modal
			document.getElementById("solution-detail-modal").style.display='block';
		});
	}

function solutionsShow(riddle_id) {	

	$.post("solutions.php",
	{
		type: "get-solutions",
		id: riddle_id,
		dataType: "json"
	},
	function(data) {
		data = jQuery.parseJSON(data);
		$('table[name=solutions] tbody').attr('riddleid',riddle_id).empty();

		if (data.length > 0) {
			$("h3#riddle-solution").text(data[0].nazov);
		}

		for(var i = 0; i < data.length; ++i) {
			var row = "<tr key=\""+data[i].ID_riesenie+"\" class=\"solutionsRow"+i+"\">";
			
			row += "<td >"+ data[i].meno +"</td>";
			row += "<td >"+ data[i].popis_riesenia +"</td>";
			row += "<td >"+ data[i].najlepsi_cas +"</td>";

			$("table[name=solutions] > tbody").append(row);
		}

		document.getElementById("solution-modal").style.display='block';
	});	
	
		
}

	
</script>
