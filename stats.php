<?php

require_once "db.php";

$db = null;

function solved_riddles_ratio() {
	global $db;

	$user = dotazSQL('select * from zberatel where email="'.$_COOKIE['user'].'"', $db);
	$user = mysql_fetch_assoc($user);
	$solved_riddles = dotazSQL(
		'select COUNT(ID_hlavolam) as cnt from hlavolam left join riesenie on hlavolam.ID_hlavolam=riesenie.hlavolam where id_vlastnik="'.$user['ID_zberatel'].'" group by hlavolam.ID_hlavolam'
		, $db);
	
	$solved = mysql_fetch_assoc($solved_riddles);

	$all_riddles = dotazSQL(
		'select COUNT(ID_hlavolam) as cnt from hlavolam where id_vlastnik="'.$user['ID_zberatel'].'"'
		, $db);

	$all = mysql_fetch_assoc($all_riddles);
	
	$data = (int)(((float)$solved['cnt'] / (float)$all['cnt'])*100);
	echo json_encode($data);
	exit();
}

function lent_riddles_ratio() {
	global $db;

	$user = dotazSQL('select * from zberatel where email="'.$_COOKIE['user'].'"', $db);
	$user = mysql_fetch_assoc($user);
	$lent_riddles = dotazSQL(
		'select COUNT(hlavolam.ID_hlavolam) as cnt from hlavolam join pozicky on hlavolam.ID_hlavolam=pozicky.id_hlavolam where id_vlastnik="'.$user['ID_zberatel'].'" AND pozicky.datum_navratu is null'
		, $db);
	
	$lent = mysql_fetch_assoc($lent_riddles);

	$all_riddles = dotazSQL(
		'select COUNT(ID_hlavolam) as cnt from hlavolam where id_vlastnik="'.$user['ID_zberatel'].'"'
		, $db);

	$all = mysql_fetch_assoc($all_riddles);

	//echo json_encode($fetched_count);
	//echo "lent:".$lent['cnt']." all:".$all['cnt']."<br>";
	$data = (int)(((float)$lent['cnt'] / (float)$all['cnt'])*100);
	echo json_encode($data);
	exit();
}

function riddles_count() {
	global $db;

	$user = dotazSQL('select * from zberatel where email="'.$_COOKIE['user'].'"', $db);
	$user = mysql_fetch_assoc($user);	

	$lent_riddles = dotazSQL(
		'select COUNT(hlavolam.ID_hlavolam) as cnt from hlavolam join pozicky on hlavolam.ID_hlavolam=pozicky.id_hlavolam where id_vlastnik="'.$user['ID_zberatel'].'" AND pozicky.datum_navratu is null'
		, $db);

	$data = mysql_fetch_assoc($lent_riddles);

	$count = (int)$data['cnt'];
	echo json_encode($count);
	exit();
}

function average_difficult() {
	global $db;

	$user = dotazSQL('select * from zberatel where email="'.$_COOKIE['user'].'"', $db);
	$user = mysql_fetch_assoc($user);

	$diff1 = dotazSQL(
		'select AVG(obtiaznost_priestorova) as avgdiff from hlavolam where id_vlastnik="'.$user['ID_zberatel'].'"'
		, $db);

	$data1 = mysql_fetch_assoc($diff1);

	$diff2 = dotazSQL(
		'select AVG(obtiaznost_casova) as avgdiff from hlavolam where id_vlastnik="'.$user['ID_zberatel'].'"'
		, $db);

	$data2 = mysql_fetch_assoc($diff2);

	$diff3 = dotazSQL(
		'select AVG(obtiaznost_vedomostna) as avgdiff from hlavolam where id_vlastnik="'.$user['ID_zberatel'].'"'
		, $db);

	$data3 = mysql_fetch_assoc($diff3);

	$data = (int)(((float)$data1['avgdiff'] + (float)$data2['avgdiff'] + (float)$data3['avgdiff']) / 3);

	echo json_encode($data);
	exit();
}

if($_SERVER["REQUEST_METHOD"] == "POST") {
		// pripojenie do db
		$db = prechodneSpojeniSRBD();		
		if($_POST['type'] == 'get-solved-riddles-ratio')
			solved_riddles_ratio();
		else if($_POST['type'] == 'get-lent-riddles-ratio')
			lent_riddles_ratio();
		else if($_POST['type'] == 'get-average-difficult')
			average_difficult();
		else if($_POST['type'] == 'get-riddles-count')
			riddles_count();
	}

?>

