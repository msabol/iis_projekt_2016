<?php

# function zobrazitChybu() výpis chybového hlášení
// vstupní parametry:   nejsou, neboť chyba je v mysql_error
// návratová hodnota:   není
function zobrazitChybu() {
	if (mysql_errno() || mysql_error()) {   // existuje-li hlášení od MySQL
		trigger_error('chybaMySQL: ' .mysql_errno().': '.mysql_error(),E_USER_ERROR); // výpis hlášení MySQL, úroveň uživatelské chyby
	} // if
	else {  // nejde o chybu MySQL
		trigger_error('nelzePripojit', E_USER_ERROR);   // nejde o chybu MySQL, úroveň uživatelské chyby
	} // else
} // zobrazitChybu

# function spojeniSRBD() zajistí spojení se SŘBD
// vstupní parametry:
// návratová hodnota: otevřené spojení, pokud se podaří
function spojeniSRBD() {
	if (!($SRBD = @ mysql_pconnect('localhost:/var/run/mysql/mysql.sock', 'xsedlo02', 'eja5cave'))) { // otevření spojení
		zobrazitChybu();
	} // if SŘBD
	if (!mysql_select_db('xsedlo02', $SRBD)) { // otevření databáze
		zobrazitChybu();
	}
	return $SRBD;	
} // of spojeniSRBD

# function prechodneSpojeniSRBD() zajistí spojení se SŘBD
//
// vstupní parametry:
// návratová hodnota: otevřené přechodné spojení, pokud se podaří
// globální proměnné:
function prechodneSpojeniSRBD() {
	if (!($SRBD = @ mysql_connect('localhost:/var/run/mysql/mysql.sock', 'xsedlo02', 'eja5cave'))) { // otevření spojení
		zobrazitChybu();
	} // if SŘBD
	if (!mysql_select_db('xsedlo02', $SRBD)) { // otevření databáze
		zobrazitChybu();
	}
	return $SRBD;
} // of prechodneSpojeniSRBD

# function dotazSQL() provedení jednoho dotazu SQL a případné ošetření chyby
// vstupní parametry:   $retezecSQL - prováděný dotaz
//                      $SRDB - otevřený SŘBD
// návratová hodnota:   vytvořená tabulka nebo false
function dotazSQL($retezecSQL, $SRBD) {
	if (!($vysledek =  @ mysql_query($retezecSQL, $SRBD))) {

		echo 'kritickyDotaz: '.$retezecSQL."\n";
		zobrazitChybu();   // hlášení chyby v případě chybného provedení dotazu
	} // if !result
	return $vysledek;
} // of dotazSQL

?>
