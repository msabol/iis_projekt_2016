<?php
	// redirect na login pokial nikto nieje prihlaseny
	if(!isset($_COOKIE['user']) || $_COOKIE['user'] != 'admin' ) {
		header('Location: login.php');	// odkomentovat az vsetko pojde
	}

	include "db.php";

	$db = null;

	function logout() {
		// unset cookie
		setcookie("user", "", time()-3600);
		exit();
	}

	function users() {
		global $db;

		$users = dotazSQL('select * from zberatel', $db);
		$data = array();
		while ($row = mysql_fetch_assoc($users)) {
			   $data[] = $row;
		}

		echo json_encode($data);
		exit();
	}

	function flip_active() {
		global $db;

		$email = $_POST['email'];
		$novaAktivita;

		$user = dotazSQL('select * from zberatel where email="'.$email.'"', $db);
		$user = mysql_fetch_assoc($user);
		if($user['aktivny'] == 'y') 
			$novaAktivita = 'n';
		else
			$novaAktivita = 'y';
		
		if(dotazSQL('update zberatel set aktivny="'.$novaAktivita.'" where email="'.$email.'"', $db)) {
			echo json_encode(array('status' => 'ok', 'aktivny' => $novaAktivita));
			exit();
		} else {
			echo json_encode(array('status' => 'err'));
			exit();
		}	
	}

	function delete_user() {
		global $db;

		$email = $_POST['email'];

		if(dotazSQL("delete from zberatel where email='".$email."'", $db)) {
			echo json_encode(array('status' => 'ok'));
			exit();
		} else {
			echo json_encode(array('status' => 'error'));
			exit();
		}
	}

	if($_SERVER["REQUEST_METHOD"] == "POST") {
		// pripojenie do db
		$db = prechodneSpojeniSRBD();	
		if($_POST['type'] == 'get-users')
			users();
		else if($_POST['type'] == 'logout')
			logout();
		else if($_POST['type'] == 'flip-active')
			flip_active();
		else if($_POST['type'] == 'delete-user')
			delete_user();
	}
	
?>

<!DOCTYPE html>
<html language="sk-SK">
<head>
    <title>Riddle book - admin</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta charset="ISO-8859-2">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
    <link rel="stylesheet" href="w3.css">
</head>
<style>
    .ui {
        padding: 0px;
        display: none;
    }
</style>
<body>

<div class="w3-container" style="margin-left:130px">

    <div class="w3-container">
	<div class="w3-container">
		<h2 style="float: left;">Spr�va u��va�e�ov</h2>
		<h3><button onclick="logout();" class="w3-btn w3-round-large w3-white w3-border w3-border-blue" style="float: right;">Logout</button></h3>
	</div>	
        <div class="w3-row toggle-colour">
            <a href="javascript:void(0)" onclick="openUi(event, 'Users');">
                <div class="w3-third tablink w3-bottombar w3-hover-light-grey w3-padding  w3-border-teal">U��vatelia</div>
            </a>
        </div>

        <div id="Users" class="w3-container ui" style="display:block">
            <div class="w3-row w3-padding-4">
                <input class="w3-threequarter" id="filter" type="text" class="w3-input" placeholder="Search.."
                       onkeyup="filterContent('Users')">
                <button class="w3-btn w3-quarter">Reload</button>
            </div>
            <table class="w3-table-all w3-hoverable" name="users">
                <thead>
                <tr class="w3-light-grey">
                    <th>Meno</th>
		    <th>Email</th>
                    <th>Aktivovan�</th>	
		    <th>Akcie</th>
                </tr>
                </thead>
		<tbody>
		</tbody>
            </table>


        </div>
    </div>

</div>



<script>
	$(document).ready(function(){
		// pouzivatelove hadanky
		$.post("admin.php",
		{
			type: "get-users",
			dataType: "json"
		},
		function(data) {
			data = jQuery.parseJSON(data);
			for(var i = 0; i < data.length; ++i) {
				var row = "<tr class=\"trUser"+i+"\">"

				row += "<td>"+ data[i].jmeno +"</td>";
				row += "<td>"+ data[i].email +"</td>";
				row += "<td class=\"tdAktivita"+i+"\">"+ (data[i].aktivny.localeCompare('y') == 0 ? 'V' : 'X') +"</td>";
				var akciaNazov;
				if(data[i].aktivny.localeCompare('y') == 0)
					akciaNazov = "Deaktivova�";
				else
					akciaNazov = "Aktivova�";

				row += '<td>'+
					'<div style="width: 150px; float: left;">'+
					'<button class="btnAktivita'+i+' w3-btn-block w3-blue" onclick="flipActive(\''+data[i].email+'\', '+i+')">'+akciaNazov+'</button>'+
					'</div>'+
					'<button class="w3-btn w3-red" onclick="deleteUser(\''+data[i].email+'\', '+i+')\">Zmaza�</button>'+
					'</td>';

				$("table[name=users] > tbody").append(row);
			}
		});
	});

	function flipActive(a_email, a_row) {
		$.post("admin.php",
		{
			type: "flip-active",
			dataType: "json",
			email: a_email
		},
		function(data) {
			data = jQuery.parseJSON(data);
			if(data.status.localeCompare("ok") == 0) {
				$(".tdAktivita"+a_row).text((data.aktivny.localeCompare("y") == 0 ? "V" : "X"));
				$(".btnAktivita"+a_row).text((data.aktivny.localeCompare("y") == 0 ? "Deaktivova�" : "Aktivova�"));
			}
		});
	}

	function deleteUser(a_email, a_row) {
		$.post("admin.php",
		{
			type: "delete-user",
			dataType: "json",
			email: a_email
		},
		function(data) {
			data = jQuery.parseJSON(data);
			if(data.status.localeCompare("ok") == 0) {
				$(".trUser"+a_row).hide();
			}
		});
	}

	function logout() {
		$.post("index.php",
		{
			type: "logout"
		},
		function(data) {
			window.location.href = "login.php"
		});
	}

    function openUi(evt, ui) {
        var i, x, tablinks;
        x = document.getElementsByClassName("ui");
        for (i = 0; i < x.length; i++) {
            x[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("toggle-colour")[0].getElementsByClassName("tablink");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" w3-border-teal", "");
        }
        document.getElementById(ui).style.display = "block";
        evt.currentTarget.firstElementChild.className += " w3-border-teal";
    }

    function filterContent(tableId) {
        var input, filter, table, tr, td, i;
        input = document.getElementById(tableId).getElementsByTagName("input")[0];
        filter = input.value.toUpperCase();
        table = document.getElementById(tableId).getElementsByTagName("table")[0];
        tr = table.getElementsByTagName("tr");
        for (i = 0; i < tr.length; i++) {
            td = tr[i].getElementsByTagName("td")[0];
            if (td) {
                if (td.innerHTML.toUpperCase().indexOf(filter) > -1) {
                    tr[i].style.display = "";
                } else {
                    tr[i].style.display = "none";
                }
            }
        }
    }


</script>

</body>
</html>

