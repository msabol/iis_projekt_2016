<?php

	require_once "db.php";

	$db = null;

	function submit_leaser() {
		global $db;

		$jmeno = $_POST['jmeno'];

		dotazSQL('insert into vypozicajuci (meno)
					values ("' . $jmeno . '")', $db);

		exit();
	}

	function lend_detail() {
		global $db;
		
		$id = $_POST['id'];
		
		$pozicky = dotazSQL(
			"select *, now() as now from pozicky join hlavolam on pozicky.id_hlavolam=hlavolam.ID_hlavolam join vypozicajuci on vypozicajuci.ID_vypozicajuci=pozicky.id_vypozicajuci where ID_pozicka=\"".$id."\"", 
			$db);

		$data = mysql_fetch_assoc($pozicky);

		echo json_encode($data);
		exit();
	}	

	function lend_edit() {
		global $db;

		$json = $_POST['new_data'];
		$json = str_replace("\\\"", "\"", $json);	// nutna uprava, ciste by to json_decode neprechrumal

		$data = json_decode($json);

		// update pujcky
		dotazSQL("update pozicky set id_vypozicajuci='".$data->id_vypozicajuci.
			"', id_hlavolam='".$data->id_hlavolam.
			"', datum_pozicky='".$data->datum_pozicky.
			"', datum_navratu='".$data->datum_navratu.
			"', uspesne_vyriesene='".$data->uspesne_vyriesene.
			"' where ID_pozicka=".$data->ID_pozicka,
			$db);

		exit();
	}

	function lend_return() {
		global $db;

		$json = $_POST['new_data'];
		$json = str_replace("\\\"", "\"", $json);	// nutna uprava, ciste by to json_decode neprechrumal

		$data = json_decode($json);

		// update pujcky
		dotazSQL("update pozicky set id_vypozicajuci='".$data->id_vypozicajuci.
			"', id_hlavolam='".$data->id_hlavolam.
			"', datum_pozicky='".$data->datum_pozicky.
			"', datum_navratu='".$data->datum_navratu.
			"', uspesne_vyriesene='".$data->uspesne_vyriesene.
			"' where ID_pozicka=".$data->ID_pozicka,
			$db);

		exit();
	}

	if($_SERVER["REQUEST_METHOD"] == "POST") {
		// pripojenie do db
		$db = prechodneSpojeniSRBD();		
		if($_POST['type'] == 'lend-detail')
			lend_detail();
		else if($_POST['type'] == 'lend-edit')
			lend_edit();
		else if($_POST['type'] == 'lend-return')
			lend_return();
		else if ($_POST['type'] == 'submit-leaser')
			submit_leaser();
	}

?>

<div class="w3-modal" id="lend-detail-modal" style="display:none">
	<div class="w3-modal-content w3-card-12">
		<header class="w3-container w3-teal w3-padding">
			<span class="w3-closebtn w3-hover-text-black"
				onclick="document.getElementById('lend-detail-modal').style.display='none'">X</span>
			<h3>Detail p��i�ky</h3>
	</header>
	<div class="w3-container">		
		<div id="lend-main-info">
			<h5>Vypo�i�aj�ci:</h5>
			<p id="lend-vypujcil"></p><br>
			<h5>N�zov hlavolamu:</h5>
			<p id="lend-hlavolam"></p><br>
			<h5>D�tum p��i�ky:</h5>
			<p id="lend-pujcka"></p><br>
			<h5>D�tum n�vratu:</h5>
			<p id="lend-navrat"></p><br>
			<h5>�spe�ne vyrie�en�:</h5>
			<p id="lend-vyresene"></p><br>
		</div>
	</div>

	</div>
</div>

<div class="w3-modal" id="lend-edit-modal" style="display:none">
	<div class="w3-modal-content w3-card-12">
		<header class="w3-container w3-teal w3-padding">
			<span class="w3-closebtn w3-hover-text-black"
				onclick="document.getElementById('lend-edit-modal').style.display='none'">X</span>
			<h3>�prava p��i�ky</h3>
	</header>
	<div class="w3-container">

		<div id="lend-main-edit">
			<h5>Vypo�i�aj�ci:</h5>
			<p id="edit-id_vypozicajuci"></p><br>
			<h5>N�zov hlavolamu:</h5>
			<p id="edit-id_hlavolam"></p><br>
			<h5>D�tum p��i�ky:*</h5>
			<input type="text" id="edit-datum_pozicky" required><br>
			<h5>D�tum n�vratu:</h5>
			<input type="text" id="edit-datum_navratu"><br>
			<h5>�spe�ne vyrie�en�:</h5>
			<input type="text" id="edit-uspesne_vyriesene"><br>
		</div>
	</div>
	<footer class="w3-container w3-teal w3-padding">
		<div class="btn-group">
			<button class="w3-btn w3-green" id="accept-edit">Upravi�</button>
			<button class="w3-btn w3-red" id="cancel-edit" onclick="document.getElementById('lend-edit-modal').style.display='none';">Zru�i�</button>
		</div>
	</footer>
	</div>
</div>

<div class="w3-modal" id="lend-return-modal" style="display:none">
	<div class="w3-modal-content w3-card-12">
		<header class="w3-container w3-teal w3-padding">
			<span class="w3-closebtn w3-hover-text-black"
				onclick="document.getElementById('lend-return-modal').style.display='none'">X</span>
			<h3>Vr�ti�</h3>
	</header>
	<div class="w3-container">

		<div id="lend-main-return">
			<h5>Vypo�i�aj�ci:</h5>
			<p id="return-id_vypozicajuci"></p><br>
			<h5>N�zov hlavolamu:</h5>
			<p id="return-id_hlavolam" ></p><br>
			<h5>D�tum p��i�ky:</h5>
			<p id="return-datum_pozicky"></p><br>
			<h5>D�tum n�vratu:*</h5>
			<input type="text" id="return-datum_navratu" required><br>
			<h5>�spe�ne vyrie�en�:</h5>
			<input type="text" id="return-uspesne_vyriesene"><br>
		</div>
	</div>
	<footer class="w3-container w3-teal w3-padding">
		<div class="btn-group">
			<button class="w3-btn w3-green" id="accept-return">Vr�ti�</button>
			<button class="w3-btn w3-red" id="cancel-return" onclick="document.getElementById('lend-return-modal').style.display='none';">Zru�i�</button>
		</div>
	</footer>
	</div>
</div>

<div class="w3-modal" id="leaser-add-modal" style="display:none">
	<div class="w3-modal-content w3-card-12">
		<header class="w3-container w3-teal w3-padding">
				<span class="w3-closebtn w3-hover-text-black"
					  onclick="$('#leaser-add-modal').hide()">X</span>
			<h3>Nov� vypo�i�aj�ci</h3>
		</header>
		<form class="w3-container" name="form-leaser-add" action="javascript:submitLeaser()" method="POST">
			<div class="w3-section">
				<label><b>Meno a priezvisko*</b></label>
				<input class="w3-input w3-border w3-margin-bottom" type="text" name="jmeno" required>		
			</div>
			<button class="w3-btn-block w3-green w3-section w3-padding" type="submit">Pridaj</button>
		</form>
	</div>
</div>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript">

function submitLeaser()
	{
		var data = $("form[name=form-leaser-add]").serializeArray();
		$.post("lends.php",
			{
				dataType: "json",
				type: "submit-leaser",
				jmeno: data[0].value

			},
			function(data) {
				document.getElementById("leaser-add-modal").style.display='none';
			});
	}


	function addLeaser(riddle_id) {
		document.getElementById("leaser-add-modal").style.display='block';
	}

	function lendDetail(lend_id) {
		$.post("lends.php",
			{
			type: "lend-detail",
			id: lend_id,
			dataType: "json"
		}, 
		function(data) {
			data = jQuery.parseJSON(data);

			// zakladne info
			$("p#lend-vypujcil").text(data.meno);
			$("p#lend-hlavolam").text(data.nazov);
			$("p#lend-pujcka").text(data.datum_pozicky);
			$("p#lend-navrat").text(data.datum_navratu);
			$("p#lend-vyresene").text(data.uspesne_vyriesene);

			// display modal
			document.getElementById("lend-detail-modal").style.display='block';
		});
	}

	function lendEdit(lend_id, lend_row) {
		$.post("lends.php",
			{
			type: "lend-detail",
			id: lend_id,
			dataType: "json"
		}, 
		function(data) {
			data = jQuery.parseJSON(data);

			// zakladne info
			$("p#edit-id_vypozicajuci").text(data.meno);
			$("p#edit-id_hlavolam").text(data.nazov);
			$("input#edit-datum_pozicky").val(data.datum_pozicky);
			$("input#edit-datum_navratu").val(data.datum_navratu);
			$("input#edit-uspesne_vyriesene").val(data.uspesne_vyriesene);

			// nastav funkciu upravenia
			$("button#accept-edit").click(function() {
				// zober vsetky data spat do data a zakoduj do JSON
				data.datum_pozicky = $("input#edit-datum_pozicky").val();

				data.datum_navratu = $("input#edit-datum_navratu").val();
				// if(data.obtiaznost_priestorova < 0) data.obtiaznost_priestorova = 0;
				// else if(data.obtiaznost_priestorova > 100) data.obtiaznost_priestorova = 100;

				data.uspesne_vyriesene = $("input#edit-uspesne_vyriesene").val();

				var json_data = JSON.stringify(data);

				$.post("lends.php",
				{
					type: 'lend-edit',
					dataType: 'json',
					contentType: "application/json",
					new_data: json_data
				},
				function(){
					// zavri modal	
					document.getElementById("lend-edit-modal").style.display='none';

					// refresh stranky
					//document.location.href = "index.php";
					fetchLends();
					updateStats();
				});
			});

			// display modal
			document.getElementById("lend-edit-modal").style.display='block';
		});
	}

	function lendReturn(lend_id, lend_row) {
		$.post("lends.php",
			{
			type: "lend-detail",
			id: lend_id,
			dataType: "json"
		}, 
		function(data) {
			data = jQuery.parseJSON(data);

			// zakladne info
			$("p#return-id_vypozicajuci").text(data.meno);
			$("p#return-id_hlavolam").text(data.nazov);
			$("p#return-datum_pozicky").text(data.datum_pozicky);
			$("input#return-datum_navratu").val(data.now);
			$("input#return-uspesne_vyriesene").val(data.uspesne_vyriesene);
			
			// nastav funkciu upravenia
			$("button#accept-return").click(function() {
				// zober vsetky data spat do data a zakoduj do JSON

				data.datum_navratu = $("input#return-datum_navratu").val();
				data.uspesne_vyriesene = $("input#return-uspesne_vyriesene").val();
				// if(data.obtiaznost_priestorova < 0) data.obtiaznost_priestorova = 0;
				// else if(data.obtiaznost_priestorova > 100) data.obtiaznost_priestorova = 100;

				var json_data = JSON.stringify(data);

				$.post("lends.php",
				{
					type: 'lend-return',
					dataType: 'json',
					contentType: "application/json",
					new_data: json_data
				},
				function(){
					// zavri modal	
					document.getElementById("lend-return-modal").style.display='none';

					// refresh stranky
					document.location.href = "index.php";
				});
			});

			// display modal
			document.getElementById("lend-return-modal").style.display='block';
		});
	}
</script>
