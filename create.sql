
CREATE TABLE zberatel (
    ID_zberatel bigint NOT NULL PRIMARY KEY AUTO_INCREMENT,
    jmeno varchar(128) NOT NULL,
    email varchar(128) NOT NULL,
    heslo varchar(100) NOT NULL,
    aktivovany varchar(1) NOT NULL
);

CREATE TABLE puzzle (
    ID_puzzle bigint NOT NULL PRIMARY KEY AUTO_INCREMENT,
    pocet_dielov bigint,
    autor varchar(50),
    tema varchar(25)
);

CREATE TABLE hadanka (
    ID_hadanka bigint NOT NULL PRIMARY KEY AUTO_INCREMENT,
    druh varchar(30),
    jazyk varchar(3),
    narocnost_lingvisticka bigint
);

CREATE TABLE hlavolam (
    ID_hlavolam bigint NOT NULL PRIMARY KEY AUTO_INCREMENT,
    typ varchar(64) NOT NULL,
    nazov varchar(128) NOT NULL,
    krajina_povodu varchar(3),
    je_puzzle bigint NULL,
    je_hadanka bigint NULL,
    obtiaznost_priestorova bigint,
    obtiaznost_casova bigint,
    obtiaznost_vedomostna bigint,
    id_vlastnik bigint
);

CREATE TABLE vypozicajuci (
    ID_vypozicajuci bigint NOT NULL PRIMARY KEY AUTO_INCREMENT,
    meno varchar(50) NOT NULL
);

CREATE TABLE riesenie (
    ID_riesenie bigint NOT NULL PRIMARY KEY AUTO_INCREMENT,
    hlavolam bigint NOT NULL,
    vypozicajuci bigint NOT NULL,
    popis_riesenia nvarchar(1000),
    najlepsi_cas bigint
);

CREATE TABLE pozicky (
    ID_pozicka bigint NOT NULL PRIMARY KEY AUTO_INCREMENT,
    id_vypozicajuci bigint NOT NULL,
    id_zberatel bigint NOT NULL,
    datum_pozicky datetime NOT NULL,
    datum_navratu datetime,
    uspesne_vyriesene char(1),
    id_hlavolam bigint NOT NULL
);

ALTER TABLE hlavolam ADD CONSTRAINT FK_je_puzzle FOREIGN KEY(je_puzzle) REFERENCES puzzle(ID_puzzle) on delete cascade ;
ALTER TABLE hlavolam ADD CONSTRAINT FK_je_hadanka FOREIGN KEY(je_hadanka) REFERENCES hadanka(ID_hadanka) on delete cascade ;
ALTER TABLE hlavolam ADD CONSTRAINT FK_vlastnik FOREIGN KEY(id_vlastnik) REFERENCES zberatel(ID_zberatel);
ALTER TABLE riesenie ADD CONSTRAINT FK_hlavolam FOREIGN KEY(hlavolam) REFERENCES hlavolam(ID_hlavolam);
ALTER TABLE riesenie ADD CONSTRAINT FK_vypozicajuci_riesenie FOREIGN KEY(vypozicajuci) REFERENCES vypozicajuci(ID_vypozicajuci);
ALTER TABLE pozicky ADD CONSTRAINT FK_vypozicajuci_pozicky FOREIGN KEY(id_vypozicajuci) REFERENCES vypozicajuci(ID_vypozicajuci);
ALTER TABLE pozicky ADD CONSTRAINT FK_vypozicajuci_zberatel_pozicky FOREIGN KEY(id_zberatel) REFERENCES zberatel(ID_zberatel);
ALTER TABLE pozicky ADD CONSTRAINT FK_hlavolam_pozicky FOREIGN KEY(id_hlavolam) REFERENCES hlavolam(ID_hlavolam);

-- alter table hlavolam drop foreign key FK_je_puzzle;
-- alter table hlavolam drop foreign key FK_je_hadanka;